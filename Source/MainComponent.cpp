/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    addAndMakeVisible (midiLabel);
    midiLabel.setText("testLabel", dontSendNotification);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(10, 10, 100, 100);
}

void MainComponent::handleIncomingMidiMessage(MidiInput* midiInput, const MidiMessage& message)
{
    DBG ("Midi Function");
    String midiText;
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << "\n:Number" << message.getNoteNumber();
        midiText << "\n:Velocity" << message.getVelocity();
    }
    midiLabel.getTextValue() = midiText;
}
